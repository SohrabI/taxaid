
const csv=require('csvtojson')

const fs = require('fs');

var tradesFilePath=`./TaxReturn/MainCryptopia-Trades.csv`
var line = 1;
var row 

csv()
.fromFile(tradesFilePath)
.then((tradesObj)=>{
    for (let i=0; i<tradesObj.length; i++){
        var tradeDate = tradesObj[i].Date;
        var tradeTime = tradesObj[i].Time;
        var quoteAsset = tradesObj[i].QuoteAsset;
        if (quoteAsset === "BTC"){
            getBTCPrice(1, tradeDate, tradeTime)
            .then((price)=>{
                console.log(`Adding ${price} to row ${i}`);
            })
            .catch((error)=>{
                console.log(error);
            }) 
        }
        else if (quoteAsset === "ETH"){
            getETHPrice(1, tradeDate, tradeTime)
            .then((price)=>{
                addPrice(i, price); 
            })
            .catch((error)=>{
                console.log(error);
            }) 
        }      
    }
})

function getBTCPrice(filenumber, tDate, tTime) {
    return new Promise((resolve, reject) => {
        var csvFilePath=`./PriceHistory/BTCUSD/${filenumber}.csv`;
        csv()
        .fromFile(csvFilePath)
        .then((jsonObj)=>{
            for (let j=0; j<jsonObj.length; j++){
                if (jsonObj[j].Date == tDate && jsonObj[j].Time >= tTime){
                    console.log(jsonObj[j].price);                
                    resolve(jsonObj[j].price);
                }
                else if (jsonObj[j].Date > tDate){
                    console.log(jsonObj[j].price);     
                    resolve(jsonObj[j].price);
                }           
            }
            console.log(`File number ${filenumber} searched.`);
            filenumber++;
            if (filenumber <= 54){
                getBTCPrice(filenumber);
            }
            else {
                reject("Epic Fail");
            }
        })
    })
}

function getETHPrice(filenumber, tDate, tTime) {
    return new Promise((resolve, reject) => {
        var csvFilePath=`./PriceHistory/ETHUSD/${filenumber}.csv`
        csv()
        .fromFile(csvFilePath)
        .then((jsonObj)=>{
            for (let j=0; j<jsonObj.length; j++){
                if (jsonObj[j].Date == tDate && jsonObj[j].Time >= tTime){
                    console.log(jsonObj[j].price);                
                    resolve(jsonObj[j].price);
                }
                else if (jsonObj[j].Date > tDate){
                    console.log(jsonObj[j].price);     
                    resolve(jsonObj[j].price);
                }           
            }
            console.log(`File number ${filenumber} searched.`);
            filenumber++;
            if (filenumber <= 31){
                getBTCPrice(filenumber);
            }
            else {
                reject("Epic Fail.")
            }
        })
    })
}


//function addPrice(placeinarray, price){
 //   var json = JSON.stringify(complete, null, 2);
 //   fs.writeFileSync('Part1.json', json);
 //   console.log(`Adding ${price} to row ${placeinarray}`);
//}


